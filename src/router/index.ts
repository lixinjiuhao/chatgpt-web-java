import type { App } from 'vue'
import type { RouteRecordRaw } from 'vue-router'
import { createRouter, createWebHashHistory } from 'vue-router'
import { setupPageGuard } from './permission'
import { ChatLayout } from '@/views/chat/layout'
import { appChatLayout } from '@/views/apiChat/layout'
import NoConfigLayout from '@/views/myLayouts/NoConfigLayout.vue';

const routes: RouteRecordRaw[] = [
  {
    path: '/',
    name: 'Root',
    component: ChatLayout,
    redirect: '/chat',
    children: [
      {
        path: '/chat/:uuid?',
        name: 'Chat',
        component: () => import('@/views/chat/index.vue'),
      },
    ],
  },


	// 用于api测试的窗口
	{
		path: '/api',
		name: 'ApiChat',
		component: appChatLayout,
		children: [
			{
				path: '/api/chat/:uuid?',
				name: 'apiChat',
				component: () => import('@/views/apiChat/index.vue'),
			},
		],
	},
	{
		path: '/root/chat/:uuid?',
		name: 'chatRoot',
		component: () => import('@/views/chatRoot/index.vue'),
	},
  {
    path: '/login',
    name: 'Login',
    component: () => import('@/views/login/index.vue'),
  },
	{
		path: '/draw',
		name: 'Draw',
		component: () => import('@/views/draw/index.vue'),
	},
  {
    path: '/regist',
    name: 'regist',
    component: () => import('@/views/regist/index.vue'),
  },
	{
		path: '/root',
		name: 'root',
		component: () => import('@/views/chatRoot/root/index.vue'),
	},
	{
		path: '/chatBase',
		name: 'chatBase',
		component: () => import('@/views/chatRoot/root/chatBase.vue'),
	},
	{
		path: '/pay/',
		name: 'Pay',
		component: () => import('@/views/pay/index.vue'),
	},
	{
		path: '/payOrder/:orderId',
		name: 'payOrder',
		component: () => import('@/views/pay/payOrder.vue'),
	},
	{
		path: '/paySuccess/:orderId',
		name: 'paySuccess',
		component: () => import('@/views/pay/paySuccess.vue'),
	},

	{
		path: '/aide',
		name: 'aide',
		component: () => import('@/views/aide/index.vue'),
	},

	{
		path: '/aide/detail',
		name: 'aideDetail',
		component: () => import('@/views/aide/details/index.vue'),
	},

  {
    path: '/404',
    name: '404',
    component: () => import('@/views/exception/404/index.vue'),
  },

  {
    path: '/500',
    name: '500',
    component: () => import('@/views/exception/500/index.vue'),
  },

  {
    path: '/:pathMatch(.*)*',
    name: 'notFound',
    redirect: '/404',
  },
]

export const router = createRouter({
  history: createWebHashHistory(),
  routes,
  scrollBehavior: () => ({ left: 0, top: 0 }),
})

setupPageGuard(router)

export async function setupRouter(app: App) {
  app.use(router)
  await router.isReady()
}
