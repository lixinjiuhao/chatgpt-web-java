import {GPT3, GPT4} from "@/api/ServiceTypeConstant";


export interface PromptRecordResult {
	allCost:string;
	page:PromptRecordPage
}


export interface PromptRecordPage {
	total:number,
	records:Array<PromptRecord>
}

/**
 * PromptRecord 类表示一个提示记录。
 */
export interface PromptRecord {
	 id: number, // 提示记录的唯一标识符
	 token: string, // 提示记录的令牌
	 source: string, // 来源
	 serviceType: string, // 服务类型
	 conversationId: string, // 对话的唯一标识符
	 relyToken: number, // 回复的令牌
	 promptToken: number, // 提示的令牌
	 cost:string // 消耗的额度
}

export interface PromptRecordReq {
	page:number,
	pageSize:number;
	model:string;
	range:[],
	startTime:bigint,
	endTime:bigint;
}

export interface CouponQueryReq {
	page:number,
	pageSize:number;
	account:string;
	useAccount:string;
	status:number;
	couponAmount:number;
	couponNo:string;
	range:[],
	startTime:bigint,
	endTime:bigint;
}



export interface CouponAddReq {
	couponAmount:number;
}

export interface CouponUseReq {
	couponNo:string;
}

export interface CouponDeleteReq {
	couponNo:string,
	account:string;
}
export interface CouponRecordResult {
	page:CouponRecordPage
}

export interface CouponRecordPage {
	total:number,
	records:Array<Coupon>
}

export interface Coupon{
	couponNo:number,
	account:number;
	useAccount:number;
	status:number;
	couponAmount:number;
	createTime:string,
	updateTime:string;
}



export const couponStatus: { label: string; key: number; value: number }[] = [
	{ label: '未使用', key: 1, value: 1 },
	{ label: '已使用', key: 0, value: 0 },
	{ label: '作废', key: -1, value: -1 },
]

