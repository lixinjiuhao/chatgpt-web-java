/**
 * 请求绘画任务
 */
export interface SubmitMJVo {
	state:string,
	prompt:string,
	fileName:string,
	model:string
}


export interface QueryDrawTask{
	page:number,
	pageSize:number;
	status:number[]
}


export interface SubmitMJTaskResult {
	taskId:string
}

export interface QueryDrawTaskResult {
	total:number,
	records: Array<TaskResult>
}

export interface TaskResult {
	id:string,
	taskId:string,
	taskUrl:string,// 生成图url
	status:string,//任务状态 0 -SUBMITTED 已提交,1-IN_PROGRESS 执行中,2-SUCCESS 完成,-1 -失败
	createTime:string,
	finishTime:string,
	type:number,
	parentId:string,
	prompt:string
	parentIndex:number
}


export interface DrawChangeRequest {
	state:string, // state: 自定义参数, task中保留.
	action:string,// 	action:string
	taskId:string,//任务ID: action 为 UPSCALE\VARIATION\RESET 必传.
	index:string,// 普通的图片的下标  1-4

}

export interface UpdateDrawTaskRequest {
	id:string,
	status:number
}

// mj的state参数  --v 5 --s 100 --ar 9:16 --c 0 --q 1
export interface DrawState {
	model:string,
	fileName:string,
	v:string,
	s:string,
	ar:string,// 比例
	c:string,
	q:string
	stateActive:boolean //控制是否携带参数
}

