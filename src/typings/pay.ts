

export interface AliPayType {
	account: string
	payType: string
}



export interface orderReq {
	orderId:string;// 订单id
	userId:string; //用户昵称
	productId:string; // 产品id
	createTime:string; //创建时间
	endTime:string; //结束时间
	status:number; // 账号状态 0正常 1：禁止登录
	categoryId:number;//产品分类
}


export interface OrderSnapshotsReq {
	orderId:string;// 订单id
	productId:string; // 产品id
	createTime:string; //创建时间
	endTime:string; //结束时间
}



export interface OrderInfo {
	id:string;// id
	orderId:string;// 订单id
	productId:string;// 产品id
	userId:string; //用户id
	payment:string; // 支付金额
	status:string; //支付状态
	createTime:string; //创建时间
	updateTime:string; //更新时间
	closeTime:string; //关闭时间
	endTime:string; //结束时间

}


export interface OrderSnapshotsInfo {
	id:string;// id
	orderId:string;// 订单id
	productId:string;// 产品id
	totalPrice:string;// 总支付金额
	currentUnitPrice:string;// 商品单价
	quantity:number;// 商品数量
	createTime:string; //创建时间
	messageCredits:string; //messageCredits
	chatbots:string; //chatbots
	characters:string; //characters
	extra:string; //扩展字段

}
