


export interface ConsumerRecordPage {
	total:number,
	records:Array<ConsumerRecordReq>
}

/**
 * 用户的消费记录
 */
export interface ConsumerRecord {
	  id: number, // 提示记录的唯一标识符
  	cost:string // 消耗的额度
		costBefore:string,
		costAfter:string,
		op:string
}

export interface ConsumerRecordReq {
	page:number,
	pageSize:number;
}

