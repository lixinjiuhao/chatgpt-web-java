import { ss } from '@/utils/storage'
const LOCAL_NAME = 'rootChatStorage'
import { v4 as uuidv4 } from 'uuid';

let botId = "blueCat"

export function defaultState(): Chat.ChatState {
	let uuid = uuidv4()
	uuid = botId+"-"+uuid
  return {
    active: uuid,
    usingContext: true,
    history: [{ uuid, title: 'New Chat', isEdit: false}],
    chat: [{ uuid, data: [] }],
  }
}

export function getLocalState(): Chat.ChatState {
  const localState = ss.get(LOCAL_NAME)
  return { ...defaultState(), ...localState }
}

export function setLocalState(state: Chat.ChatState) {
  ss.set(LOCAL_NAME, state)
}
