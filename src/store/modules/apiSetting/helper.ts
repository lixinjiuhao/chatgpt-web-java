import { ss } from '@/utils/storage'

const LOCAL_NAME = 'apiSettingsStorage'

export interface ApiSettingsState {
  systemMessage: string
  temperature: number
  top_p: number,
	token:string,
	proxyUrl:string
}

export function defaultSetting(): ApiSettingsState {
  return {
    systemMessage: 'You are ChatGPT, a large language model trained by OpenAI. Follow the user\'s instructions carefully. Respond using markdown.',
    temperature: 0.8,
    top_p: 1,
		token:"sk-123123123",
		proxyUrl:"http://www.chosen1.xyz/"
  }
}

export function getLocalState(): ApiSettingsState {
  const localSetting: ApiSettingsState | undefined = ss.get(LOCAL_NAME)
  return { ...defaultSetting(), ...localSetting }
}

export function setLocalState(setting: ApiSettingsState): void {
  ss.set(LOCAL_NAME, setting)
}

export function removeLocalState() {
  ss.remove(LOCAL_NAME)
}
