import { defineStore } from 'pinia'
import type { ApiSettingsState } from './helper'
import { defaultSetting, getLocalState, removeLocalState, setLocalState } from './helper'

export const useApiSettingStore = defineStore('api-setting-store', {
  state: (): ApiSettingsState => getLocalState(),
  actions: {
    updateSetting(settings: Partial<ApiSettingsState>) {
      this.$state = { ...this.$state, ...settings }
      this.recordState()
    },

    resetSetting() {
      this.$state = defaultSetting()
      removeLocalState()
    },

    recordState() {
      setLocalState(this.$state)
    },
  },
})
