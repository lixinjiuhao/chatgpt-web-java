import { ss } from '@/utils/storage'

const LOCAL_NAME = 'drawStateStorage'

import {DrawState} from '@/typings/draw'

export function defaultSetting(): DrawState {
	//--v 5 --s 100 --ar 9:16 --c 0 --q 1
  return {
    fileName:"",
		v:"5",
		s:"100",
		ar:"9:16",
		c:"0",
		q:"1",
		model:'Midjourney',
		stateActive:false
  }
}

export function getLocalState(): DrawState {
  const localSetting: DrawState | undefined = ss.get(LOCAL_NAME)
  return { ...defaultSetting(), ...localSetting }
}

export function setLocalState(setting: DrawState): void {
  ss.set(LOCAL_NAME, setting)
}

export function removeLocalState() {
  ss.remove(LOCAL_NAME)
}
