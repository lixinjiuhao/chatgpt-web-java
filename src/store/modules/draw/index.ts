import { defineStore } from 'pinia'
import {DrawState} from '@/typings/draw'
import { defaultSetting, getLocalState, removeLocalState, setLocalState } from './helper'

export const useDrawStore = defineStore('draw-store', {
  state: (): DrawState => getLocalState(),
  actions: {
    updateSetting(settings: Partial<DrawState>) {
      this.$state = { ...this.$state, ...settings }
      this.recordState()
    },

    resetSetting() {
      this.$state = defaultSetting()
      removeLocalState()
    },

    recordState() {
      setLocalState(this.$state)
    },
  },
})
