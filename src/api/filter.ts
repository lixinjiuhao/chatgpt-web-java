import { post } from '@/utils/request'


export function requestCheckContent<T>(content: string) {
  return post<T>({
    url: '/check/text',
    data: { "content":content },
  })
}
