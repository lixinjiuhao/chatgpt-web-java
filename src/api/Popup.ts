import { post } from '@/utils/request'
import type { NoticeInfo } from '@/store/modules/Popup/helper'

export function requestNoticeContent<NoticeInfo>(source : string) {
  return post<NoticeInfo>({
    url: '/popupInfo/getPopupInfo',
    data: {"source":source},
  })
}
