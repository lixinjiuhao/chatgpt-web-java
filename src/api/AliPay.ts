import { post,get } from '@/utils/request'
import {AliPayType, orderReq, OrderSnapshotsReq} from "@/typings/user";



export function doPay<T>(params:AliPayType) {
	return post<T>({
		url: '/alipay/alipayTradePagePay',
		data: params,
	})
}


export function queryOrder<T>(params:orderReq) {
	return post<T>({
		url: '/orderInfo/order/info',
		data: params,
	})
}

export function queryOrderSnapshot<OrderSnapshotsInfo>(params:OrderSnapshotsReq) {
	return post<OrderSnapshotsInfo>({
		url: '/orderInfo/orderSnapshots/info',
		data: params,
	})
}
