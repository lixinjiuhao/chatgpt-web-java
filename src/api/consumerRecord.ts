import { get, post, Response } from '@/utils/request'
import { ConsumerRecordPage,ConsumerRecordReq } from '@/typings/consumerRecord'

export function getConsumerRecordList(params:ConsumerRecordReq) {
	return post<ConsumerRecordPage>({
		url: '/consumptionRecords/list',
		data: params,
	})
}
