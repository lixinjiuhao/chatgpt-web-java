import {get, post, Response} from '@/utils/request'
import {
    PromptRecordResult,
    PromptRecordReq,
    CouponQueryReq,
    CouponRecordPage,
    CouponAddReq,
    CouponUseReq, CouponDeleteReq
} from '@/typings/record'

export function getCouponRecordList(params: CouponQueryReq) {
    return post<Response<CouponRecordPage>>({
        url: '/coupon/queryCoupon',
        data: params,
    })

}
export function createCoupont(params: CouponAddReq) {
        return post<Response<string>>({
            url: '/coupon/createCoupon',
            data: params,
        })
    }

export function useCoupont(params: CouponUseReq) {
        return post<Response<string>>({
            url: '/coupon/useCoupon',
            data: params,
        })
    }



export function deleteCoupont(params: CouponDeleteReq) {
    return post<Response<string>>({
        url: '/coupon/delCoupon',
        data: params,
    })
}
