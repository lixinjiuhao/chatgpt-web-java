

export const GPT3 = 'gpt-3.5-turbo'
export const GPT4 = 'gpt-4'
export const midjourney_model = "midjourney_model"

/**
 * net-gpt-3.5-turbo
 net-gpt-4
 gpt-4-v
 gpt-4-dalle
 */
export const serviceTypeOptions: { label: string; key: string; value: string }[] = [
	{ label: 'model-3.5-turbo', key: GPT3, value: GPT3 },
	{ label: 'model-3.5-turbo-16k', key: "gpt-3.5-turbo-16k", value: "gpt-3.5-turbo-16k" },
	{ label: 'model-4-0314', key: "gpt-4-0314", value: "gpt-4-0314" },
	{ label: 'model-4-0613', key: "gpt-4-0613", value: "gpt-4-0613" },
	{ label: 'net-gpt-3.5-turbo', key: "net-gpt-3.5-turbo", value: "net-gpt-3.5-turbo" },
	{ label: 'nnet-gpt-4', key: "net-gpt-4", value: "net-gpt-4" },
	{ label: 'gpt-4-v', key: "gpt-4-v", value: "gpt-4-v" },
	{ label: 'gpt-4-dalle', key: "gpt-4-dalle", value: "gpt-4-dalle" },
	// { label: 'model-4-32k',key:"gpt-4-32k",value:"gpt-4-32k"},
	{label:'claude-2',key:'claude-2',value:'claude-2'}
]

