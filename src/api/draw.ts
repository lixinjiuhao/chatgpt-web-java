import { get, post, Response } from '@/utils/request'
import {UpdateDrawTaskRequest, SubmitMJVo,SubmitMJTaskResult,TaskResult,QueryDrawTaskResult,QueryDrawTask,DrawChangeRequest} from '@/typings/draw'



export function createDrawTask(params:SubmitMJVo) {
	return post<SubmitMJTaskResult>({
		url: '/draw/createMjTask',
		data:params
	})
}


export function getDrawTask(params:QueryDrawTask) {
	return post<QueryDrawTaskResult>({
		url: '/draw/queryDrawTaskInfo',
		data:params
	})
}

export function getDrawTaskByTaskId(taskId:string) {
	return get<TaskResult>({
		url: '/draw/queryTaskInfoByTaskId?taskId='+taskId,
	})
}

export function createChangeDrawTask(params:DrawChangeRequest) {
	return post<SubmitMJTaskResult>({
		url: '/draw/getMjTask/change',
		data:params
	})
}

export function updateDrawTask(params:UpdateDrawTaskRequest) {
	return post({
		url: '/draw/updateTaskInfoStatus',
		data:params
	})
}
