import { get, post, Response } from '@/utils/request'
import { PromptRecordResult,PromptRecordReq } from '@/typings/record'

export function getPromptRecordList(params:PromptRecordReq) {
	return post<Response<PromptRecordResult>>({
		url: '/user/queryPromptRecordByToken',
		data: params,
	})
}
