import { get, post, Response } from '@/utils/request'
import {Task} from '@/typings/mj'

export function requestCreateImgTask<T>(prompt:string) {
	return post<T>({
		url: '/chat/createMjTask',
		data: { "prompt":prompt },
	})
}

export function requestGetImgByTaskId<T>(taskId:bigint) {
	return get<T>({
		url: '/chat/getMjTask?taskId='+taskId,
	})
}

export function requestGetChangeImgByTaskId<T>(taskId:bigint,index:string) {
	return get<T>({
		url: '/chat/getMjTask/change?taskId='+taskId+"&index="+index,
	})
}
